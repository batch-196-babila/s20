let inputNumber = prompt("Enter a number");
inputNumber = parseInt(inputNumber);

console.log("The number you provided is " + inputNumber);

for (inputNumber; inputNumber >= 0; inputNumber--) {
    if (inputNumber <= 50) {
        console.log("The current value is at 50. Terminating the loop")
        break;
    }

    else if (inputNumber % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number.");
        continue;    
    }

    else if (inputNumber % 5 === 0) {
        console.log(inputNumber);
    }
}

function displayMultiplicationTable() {
    for (let counter = 1; counter <= 10; counter++) {
        console.log("5 x " + counter + " = " + 5 * counter);
    }
}
displayMultiplicationTable();