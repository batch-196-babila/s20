function showMessage() {
    console.log("This is a message");
}

// showMessage();
// showMessage();
// showMessage();
// showMessage();
// showMessage();

// While loop = repeat task while condition ins true

/* 
    Syntax:
    while (condition) {
        task;
        increment/decrement;
    }
*/

let count = 5;

// while (count !== 0) {
//     showMessage();
//     count--;
// }

// while(count !== 0) {
//     console.log(count);
//     count--;
// }
/* 
1st time loop run: counter = 5 -> before we start the next loop, we decrement to 4
2nd time loop run: counter = 4 -> before we start the next loop, we decrement to 3
3rd time loop run: counter = 3 -> before we start the next loop, we decrement to 2
4th time loop run: counter = 2 -> before we start the next loop, we decrement to 1
5th time loop run: counter = 1 -> before we start the next loop, we decrement to 0 = (STOP)
*/

// Do while loops
// similar to while loop
// able to perform a task at least once  even if the condition is not true
// run atleast 1 time even the condition is not met
// run Do block before check While

// do {
//     console.log(count);
//     count--;
// } while (count === 0)

// let counterMini = 20;

// while(counterMini >= 1) {
//     console.log(counterMini);
//     counterMini--;
// }

// for loop

for(let count = 0; count <= 20; count++){
    console.log(count);
}

// let counter = 1;
for(let x = 0; x < 10; x++){
    let sum = 1 + x;
    console.log("The sum of " + 1 + " + " + x + " = " + sum);
}

// Continue and Break
// alows you to go to the next loop without finishing the current code block.

for(let counter = 0; counter <= 20; counter++){
    if(counter % 2 === 0){
        continue;
    }

    console.log(counter);

    // break
    if(counter === 1){
        break;
    }
}

for(let num = 5; num <=100; num++){
    if(num % 5 !== 0){
        continue;
    }
    console.log(num);
}

/* Mini */

for (let count = 0; count <= 100; count++) {
    if (count % 5 === 0) {
        console.log(count);
    }
}